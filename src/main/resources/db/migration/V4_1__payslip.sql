CREATE TABLE payslip(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  employee_id VARCHAR(13) NOT NULL,
  salary_id INT,
  year INT,
  month INT,
  week INT,
  date DATETIME
);
