package ie.codextech.salarymanager.type;

/**
 * @author victor.miranda@codextech.ie
 */
public enum PRSIClass {
    A,J,E,B,C,D,H,K,M,S,P;
}
