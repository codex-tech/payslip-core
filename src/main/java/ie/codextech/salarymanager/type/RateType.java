package ie.codextech.salarymanager.type;

/**
 * @author victor.miranda@codextech.ie
 */
public enum  RateType {
    USC,
    PAYE;
}
