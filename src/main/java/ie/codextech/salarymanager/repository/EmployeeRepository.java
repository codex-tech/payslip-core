package ie.codextech.salarymanager.repository;

import ie.codextech.salarymanager.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author victor.miranda@codextech.ie
 */
@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
}
