package ie.codextech.salarymanager.model;

import ie.codextech.salarymanager.type.PRSIClass;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author victor.miranda@codextech.ie
 */
@Entity
@Table(name = "employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String pps;

    private String name;

    private String surname;

    private Date startDate;

    @Enumerated(EnumType.STRING)
    private PRSIClass prsiClass;

    @OneToMany(mappedBy = "employee")
    private List<Salary> salaries;

    private Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPps() {
        return pps;
    }

    public void setPps(String pps) {
        this.pps = pps;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public PRSIClass getPrsiClass() {
        return prsiClass;
    }

    public void setPrsiClass(PRSIClass prsiClass) {
        this.prsiClass = prsiClass;
    }

    public List<Salary> getSalaries() {
        return salaries;
    }

    public void setSalaries(List<Salary> salaries) {
        this.salaries = salaries;
    }
}
