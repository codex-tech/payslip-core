package ie.codextech.salarymanager.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author victor.miranda@codextech.ie
 */
@Entity
@Table(name = "employee_tax_credit")
public class EmployeeTaxCredit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

    @OneToOne
    @JoinColumn(name = "tax_credit_id")
    private TaxCredit taxCredit;

    private Date addedDate;
}
