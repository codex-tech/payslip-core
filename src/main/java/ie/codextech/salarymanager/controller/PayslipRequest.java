package ie.codextech.salarymanager.controller;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import ie.codextech.salarymanager.type.EmployeeStatus;

import java.math.BigDecimal;

/**
 * @author victor.miranda@codextech.ie
 */
@JsonDeserialize(builder = PayslipRequest.Builder.class)
public class PayslipRequest {
    private final EmployeeStatus employeeStatus;
    private final BigDecimal baseSalary;


    public PayslipRequest(final Builder builder) {
        this.employeeStatus = builder.employeeStatus;
        this.baseSalary = builder.baseSalary;
    }

    @JsonPOJOBuilder(buildMethodName = "build", withPrefix = "with")
    public static class Builder {

        EmployeeStatus employeeStatus;
        BigDecimal baseSalary;


        public PayslipRequest build() {
            return new PayslipRequest(this);
        }

        public Builder withEmployeeStatus(EmployeeStatus val) {
            this.employeeStatus = val;
            return this;
        }

        public Builder withBaseSalary(BigDecimal val) {
            this.baseSalary = val;
            return this;
        }


    }

    public EmployeeStatus getEmployeeStatus() {
        return employeeStatus;
    }

    public BigDecimal getBaseSalary() {
        return baseSalary;
    }
}
