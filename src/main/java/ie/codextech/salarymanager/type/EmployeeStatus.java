package ie.codextech.salarymanager.type;

/**
 * @author victor.miranda@codextech.ie
 */
public enum EmployeeStatus {
    SINGLE,
    WIDOWED,
    MARRIED,
    MARRIED_BOTH_WORKING,
    LONE_PARENT
}
