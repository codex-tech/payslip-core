package ie.codextech.salarymanager.model;

import ie.codextech.salarymanager.type.TaxCreditType;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author victor.miranda@codextech.ie
 */
@Entity
@Table(name = "tax_credit")
public class TaxCredit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TaxCreditType type;

    private Integer year;

    private BigDecimal amount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TaxCreditType getType() {
        return type;
    }

    public void setType(TaxCreditType type) {
        this.type = type;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
