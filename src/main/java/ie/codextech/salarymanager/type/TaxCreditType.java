package ie.codextech.salarymanager.type;

/**
 * @author victor.miranda@codextech.ie
 */
public enum TaxCreditType {
    SINGLE_PERSON("Single Person / Married two incomes"),
    MARRIED_ONE_SALARY("Married Person or Civil Partner"),
    WIDOWED("Widowed Person or Surviving Civil Partner - qualifying for Single Person Child Carer Credit"),
    WIDOWED_WITHOUT_QUAL_CHILD("Widowed Person or Surviving Civil Partner without qualifying child"),
    WIDOWED_IN_YEAR("Widowed Person or Surviving Civil Partner in year of bereavement"),
    SINGLE_PERSON_CHILD_CARER("Single Person Child Carer Credit"),
    HOME_CARER("Home Carer Tax Credit"),
    PAYE("PAYE Employee Tax Credit"),
    EARNED_INCOME("Earned Income Tax Credit"),
    AGE_SINGLE("Age Tax Credit if Single, Widowed or Surviving Civil Partner"),
    AGE_MARRIED("Age Tax Credit if Married or in a Civil Partnership"),
    INCAPACITED_CHILD("Incapacitated Child Tax Credit"),
    DEPENDANT_RELATIVE("Dependent Relative Tax Credit"),
    BLIND_SINGLE("Blind Person's Tax Credit - Single Person"),
    BLIND_SPOUSE("Blind Person's Tax Credit - One Spouse or Civil Partner Blind"),
    BLIND_BOTH("Blind Person's Tax Credit - Both Spouses or Civil Partners Blind"),
    INCAPACITED("Incapacitated Individual");

    final String desc;

    TaxCreditType(String desc) {
        this.desc = desc;
    }
}
