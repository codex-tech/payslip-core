package ie.codextech.salarymanager.service;

import ie.codextech.salarymanager.controller.EmployeePayslipRequest;

/**
 * @author victor.miranda@codextech.ie
 */
public interface PayslipService {

    Object getPayslip(final EmployeePayslipRequest payslipRequest);
}
