package ie.codextech.salarymanager.controller;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author victor.miranda@codextech.ie
 */
public class EmployeePayslipRequest {
    private final Integer employeeId;
    private final Integer year;
    private final Integer month;

    @JsonCreator
    public EmployeePayslipRequest(@JsonProperty("employeeId") final Integer employeeId,
                                  @JsonProperty("year") final Integer year,
                                  @JsonProperty("month") final Integer month) {
        this.employeeId = employeeId;
        this.year = year;
        this.month = month;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public Integer getYear() {
        return year;
    }

    public Integer getMonth() {
        return month;
    }
}
