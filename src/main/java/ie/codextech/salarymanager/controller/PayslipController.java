package ie.codextech.salarymanager.controller;

import ie.codextech.salarymanager.service.PayslipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author victor.miranda@codextech.ie
 */
@RestController
public class PayslipController {

    private final PayslipService payslipService;

    @Autowired
    PayslipController(final PayslipService payslipService) {
        this.payslipService = payslipService;
    }

    @RequestMapping(value = "/payslip", method = RequestMethod.POST)
    @ResponseBody
    public Object getPayslip(@RequestBody final EmployeePayslipRequest payslipRequest) {
        return payslipService.getPayslip(payslipRequest);
    }

}
