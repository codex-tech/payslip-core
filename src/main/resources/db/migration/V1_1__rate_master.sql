CREATE TABLE rate(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  rate_type VARCHAR(18),
  year INT,
  start Decimal(10,2),
  end Decimal(10,2),
  rate DECIMAL(10,2)
);

