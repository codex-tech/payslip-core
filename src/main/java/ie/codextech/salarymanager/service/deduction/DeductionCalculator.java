package ie.codextech.salarymanager.service.deduction;

import ie.codextech.salarymanager.controller.PayslipRequest;
import ie.codextech.salarymanager.model.Rate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author victor.miranda@codextech.ie
 */
@Component
public class DeductionCalculator {

    public BigDecimal getRateValue(final List<Rate> rates, final BigDecimal salary) {
        return rates.stream()
                .map(rate -> DeductionCalculator.getRateAmount(rate, salary))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    static BigDecimal getRateAmount(final Rate rate, final BigDecimal salary) {
        final BigDecimal rateAmount;

        if (rate.getStart().compareTo(salary) > 0) {
            rateAmount = BigDecimal.ZERO;
        } else if (rate.getEnd() == null || rate.getEnd().compareTo(salary) >= 0) {
            rateAmount = salary.subtract(rate.getStart()).multiply(BigDecimal.valueOf(rate.getRate() / 100));
        } else {
            rateAmount = rate.getEnd().subtract(rate.getStart()).multiply(BigDecimal.valueOf(rate.getRate() / 100));
        }

        return rateAmount;
    }
}
