package ie.codextech.salarymanager.service;

import ie.codextech.salarymanager.controller.EmployeePayslipRequest;
import ie.codextech.salarymanager.model.Employee;
import ie.codextech.salarymanager.model.Salary;
import ie.codextech.salarymanager.repository.EmployeeRepository;
import ie.codextech.salarymanager.repository.RateRepository;
import ie.codextech.salarymanager.service.deduction.DeductionCalculator;
import ie.codextech.salarymanager.type.RateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author victor.miranda@codextech.ie
 */
@Service
public class PayslipServiceImpl implements PayslipService {

    private final DeductionCalculator deductionCalculator;
    private final RateRepository rateRepository;
    private final EmployeeRepository employeeRepository;

    @Autowired
    public PayslipServiceImpl(final DeductionCalculator deductionCalculator,
                              final RateRepository rateRepository, EmployeeRepository employeeRepository) {
        this.deductionCalculator = deductionCalculator;
        this.rateRepository = rateRepository;
        this.employeeRepository = employeeRepository;
    }

    public Object getPayslip(final EmployeePayslipRequest payslipRequest) {
        final Employee employee = employeeRepository.findOne(payslipRequest.getEmployeeId());

        final Salary employeeSalary = employee.getSalaries().stream()
                .filter(s -> s.getEndDate() == null).findFirst()
                .get();

        BigDecimal usc = deductionCalculator.getRateValue(
                rateRepository.getRatesByRateTypeAndYear(RateType.USC,2016), employeeSalary.getBaseSalary());
        BigDecimal paye = deductionCalculator.getRateValue(
                rateRepository.getRatesByRateTypeAndYear(RateType.PAYE,2016), employeeSalary.getBaseSalary());

        //get tax credits from employee type
        return usc;
    }
}
