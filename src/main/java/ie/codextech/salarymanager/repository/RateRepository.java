package ie.codextech.salarymanager.repository;

import ie.codextech.salarymanager.model.Rate;
import ie.codextech.salarymanager.type.RateType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author victor.miranda@codextech.ie
 */
@Repository
public interface RateRepository extends CrudRepository<Rate, Integer> {
    @Query("FROM Rate WHERE rateType = ?1 AND year = ?2")
    List<Rate> getRatesByRateTypeAndYear(final RateType type, final Integer year);
}
