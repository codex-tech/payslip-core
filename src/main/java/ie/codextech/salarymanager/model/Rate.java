package ie.codextech.salarymanager.model;

import ie.codextech.salarymanager.type.RateType;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author victor.miranda@codextech.ie
 */
@Entity
@Table(name = "rate")
public class Rate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Enumerated(EnumType.STRING)
    private RateType rateType;
    private Integer year;
    private BigDecimal start;
    private BigDecimal end;
    private Float rate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RateType getRateType() {
        return rateType;
    }

    public void setRateType(RateType rateType) {
        this.rateType = rateType;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public BigDecimal getStart() {
        return start;
    }

    public void setStart(BigDecimal start) {
        this.start = start;
    }

    public BigDecimal getEnd() {
        return end;
    }

    public void setEnd(BigDecimal end) {
        this.end = end;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }
}
