CREATE TABLE employee(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  pps VARCHAR(13) NOT NULL,
  name VARCHAR(100) NOT NULL,
  surname VARCHAR(200) NOT NULL,
  prsi_class VARCHAR(17) NOT NULL,
  start_date DATETIME
);

CREATE TABLE employee_tax_credit(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  employee_id VARCHAR(11) NOT NULL,
  tax_credit_id VARCHAR(100) NOT NULL,
  added_date DATETIME,
);

CREATE TABLE employee_salary(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  employee_id VARCHAR(11) NOT NULL,
  base_salary DECIMAL(10,2) NOT NULL,
  start_date DATETIME,
  end_date DATETIME
)