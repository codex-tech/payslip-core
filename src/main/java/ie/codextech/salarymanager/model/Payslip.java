package ie.codextech.salarymanager.model;

import javax.persistence.*;
import java.util.List;

/**
 * @author victor.miranda@codextech.ie
 */
@Entity
@Table(name = "payslip")
public class Payslip {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Integer year;

    private Integer month;

    private Integer week;

    @OneToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

    @OneToOne
    @JoinColumn(name = "salary_id")
    private Salary baseSalary;
}
