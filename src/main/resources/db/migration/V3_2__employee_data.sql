INSERT INTO employee(pps,name, surname, prsi_class, start_date) VALUES
    ('123456789012', 'Sheila', 'Fernandez', 'A', '2015-03-12');

INSERT INTO employee_salary(employee_id, base_salary, start_date, end_date) VALUES
    (1, 62700, '2015-03-12', '2016-03-01'),
    (1, 62700, '2016-03-01', null);

INSERT INTO employee_tax_credit(employee_id, tax_credit_id, added_date) VALUES
    (1, 1,'2015-03-12' ),
    (8, 1,'2015-03-12' );